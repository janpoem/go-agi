package agi

import (
	"time"
	"os"
	"runtime"
	"unsafe"
	"encoding/binary"
)

const (
	OS_LINUX = iota
	OS_MAC
	OS_WIN
	OS_OTHERS
)

// 执行的命令的路径，这里我们做一个复制，而不是引用地址，我们不能变
var fullPath, args = os.Args[0], os.Args[1:]
// 执行的目录和文件名
var dir, file, _ = RealPathSplitDirFile(fullPath)
// 当前时间
var now = time.Now()
// 时区，偏移时间
var timezone, offset = now.Zone()
// location，用于时间的解析非常重要
var location = now.Location()

var osEndian = getEndian()

func Args() []string {
	return args
}

func AppPath() string {
	return fullPath
}

func AppDir() string {
	return dir
}

func AppFile() string {
	return file
}

func AppTimeZone() string {
	return timezone
}

func AppTimeOffset() int {
	return offset
}

func AppTimeLoc() *time.Location {
	return location
}

func GetOsFlag() int {
	switch os := runtime.GOOS; os {
		case "darwin":
		return OS_MAC
		case "linux":
		return OS_LINUX
		case "windows":
		return OS_WIN
		default:
		return OS_OTHERS
	}
}

func GetOsEof() string {
	if GetOsFlag() == OS_WIN {
		return "\r\n"
	}
	return "\n"
}

func GetOsDirSpr() string {
	if GetOsFlag() == OS_WIN {
		return "\\"
	}
	return "/"
}

func GetOsEndian() binary.ByteOrder {
	return osEndian
}

// @see https://groups.google.com/forum/#!topic/golang-nuts/3GEzwKfRRQw
func isLittleEndian() bool {
	var i int32 = 0x01020304
	u := unsafe.Pointer(&i)
	pb := (*byte)(u)
	b := *pb
	return (b == 0x04)
}

func getEndian() binary.ByteOrder {
	if isLittleEndian() {
		return binary.LittleEndian
	}
	return binary.BigEndian
}