package main

import (
    "git.oschina.net/janpoem/go-agi"
    "fmt"
)

func main() {
    time, _ := agi.StrToTime("2014-12-11 12:33", agi.TF("Y-m-d H:i"))
    fmt.Println(time)
}
