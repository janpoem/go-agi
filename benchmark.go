package agi

import (
	"time"
)

func TimesTest(times int, fn func(int)) (time.Duration) {
	if times <= 0 {
		return 0
	}
	start := time.Now()
	if times == 1 {
		fn(1)
	} else {
		for i := 1; i <= times; i++ {
			fn(i)
		}
	}
	return time.Now().Sub(start)
}